import numpy as np
import geoParameters as gp
import differentiate
import geostrophicWind
import graph
import error
import accuracyExperiment

def main(number_of_points=2, N=10):
    # if (N <= 0 or N != int(N)):
    #     raise ValueError("N must be a positive integer.")
    # N = int(N)
    # if (number_of_points != 2 and number_of_points != 4):
    #     raise ValueError("Only order 2 and 4 are available")

    # # Initialise differentiation by calculating y and dy from N=10 and parameters.
    # differentiation = differentiate.differentiate(N)

    # # Calculate pressure and dpdy numerically
    # p = gp.pressure(differentiation.y)

    # if (number_of_points == 2):
    #     p_numerical = differentiation.gradient_2_point(p)
    #     graph_label = 'Two-point differences'
    #     figure_name = 'plots/twoPointDifferentiatedWind.pdf'
    # elif (number_of_points == 4):
    #     p_numerical = differentiation.gradient_4_point(p)
    #     graph_label = 'Four-point differences'
    #     figure_name = 'plots/fourPointDifferentiatedWind.pdf'

    # # Calculate u, both exactly and numerically using the geostrophic wind
    # # relation.
    # u = geostrophicWind.geostrophicWind()
    # u_numerical = u.calculate_u_from_dpdy(p_numerical)
    # u_exact = gp.uExact(differentiation.y)

    # # Plot a graph comparing exact and numerical solutions
    # graph_plot = graph.graph(label_y2=graph_label, name=figure_name)
    # graph_plot.plot_graph(differentiation.y, u_exact, u_numerical)
    
    # # Calculate error in numerical solution and plot against u
    # errors = error.error(u_exact, u_numerical)
    # errors.calculate_error()
    # errors.plot_error_graph(u_numerical)

    # Calculate mean error for values of N
    N_values = list(range(10, 501, 10))
    experiment = accuracyExperiment.accuracyExperiment()
    data = experiment.calculate_error_for_different_N(N_values, number_of_points)
    experiment.plot_loglog_graph(data)
    gradient, standard_error = experiment.calculate_linear_regression()
    print("The order of accuracy of this method of differentiation is ", \
        gradient, "with an error of", standard_error)

if __name__ == "__main__":
    main()