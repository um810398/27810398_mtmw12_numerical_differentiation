import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

import geoParameters as gp
import differentiate
import geostrophicWind
import error

class accuracyExperiment:
    '''This object helps us to answer the third part of the question. It can find\
    error for a list of N values and plots a log-log graph of this. The gradient\
    of this graph is the order of error in the differentiaion.'''

    def calculate_error_for_different_N(self, list, order):
        '''For each value of N in the list, calculate the average error associated
        with u values, dependent on order of differentiation. Input list of N \
        values and order of differentiation required, output a 2D array of errors\
        for each dy value.'''

        dy_and_errors = []

        if (order == 2):
            for N in list:
                differentiation = differentiate.differentiate(N)
                p = gp.pressure(differentiation.y)
                p_numerical = differentiation.gradient_2_point(p)
                u = geostrophicWind.geostrophicWind()
                u_2_point = u.calculate_u_from_dpdy(p_numerical)
                u_exact = gp.uExact(differentiation.y)
                mean_error = np.mean(error.error(u_exact, u_2_point).calculate_error())
                dy_and_errors.append([differentiation.dy, mean_error])
            
        elif (order == 4):
            for N in list:
                differentiation = differentiate.differentiate(N)
                p = gp.pressure(differentiation.y)
                p_numerical = differentiation.gradient_4_point(p)
                u = geostrophicWind.geostrophicWind()
                u_2_point = u.calculate_u_from_dpdy(p_numerical)
                u_exact = gp.uExact(differentiation.y)
                mean_error = np.mean(error.error(u_exact, u_2_point).calculate_error())
                dy_and_errors.append([differentiation.dy, mean_error])  
                
        return dy_and_errors

    def plot_loglog_graph(self, data):
        '''Plot graph according to initialised settings, an x and y plot, and 
        optional y2.'''
        # Input settings for graph
        font = {'size': 10}
        plt.rc('font', **font)

        # Split data into x and y values and take
        x = []
        y = [] 
        for i in range(0, len(data)):
            x.append(data[i][0])
            y.append(data[i][1])
        # Store variables for use in other functions in this object
        self.x = x
        self.y = y

        # Plot log of error against log of dy.
        plt.loglog(x, y, 'k-', marker=".")
        plt.xlabel('log(Δy)')
        plt.ylabel('log(ε)')
        plt.tight_layout()

        plt.savefig('plots/accuracyExperiment.pdf')
        plt.show()

    def calculate_linear_regression(self):
        '''Calculate the gradient, and error on gradient of the graph. This \
        tells us the order of accuracy of this method of differentiation.'''
        slope, intercept, r_value, p_value, std_err = stats.linregress(self.x, self.y)
        return slope, std_err