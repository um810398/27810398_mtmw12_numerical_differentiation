import numpy as np

import geoParameters as gp

class geostrophicWind():
    def calculate_u_from_dpdy(self, dpdy):
        '''The geostrophic wind as a function of pressure gradient'''
        if (type(dpdy) != np.ndarray):
            raise ValueError("dpdy must be an array of values")
        return -dpdy/(gp.rho*gp.f)
