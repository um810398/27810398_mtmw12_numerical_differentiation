import numpy as np 

import graph

class error:
    '''Calculates error of numerical differentiation compared to exact results.\
    Graph plotted shows how error varies in different parts of the function.'''
    def __init__(self, exact_solution, numerical_solution):
        '''Initialise exact and numerical solutions'''
        self.exact_solution = exact_solution
        self.numerical_solution = numerical_solution

    def calculate_error(self):
        '''Calculate the difference between exact and numerical solution at each \
            point of u'''
        error = np.zeros_like(self.exact_solution)

        for i in range(0, len(self.exact_solution)):
            error[i] = abs(self.exact_solution[i] - self.numerical_solution[i])
        self.error = error
        return error

    def plot_error_graph(self, x):
        '''Plot a graph of error in u against u'''
        error_graph = graph.graph('u (m/s)', 'Error in u', label_y1='Error', \
        name='plots/errorInU.pdf')
        error_graph.plot_graph(x, self.error)

