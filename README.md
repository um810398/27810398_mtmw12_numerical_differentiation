# 27810398_MTMW12_numerical_differentiation

Numerical differentiation assignment for MTMW12.

Full report can be found at []().

Libraries/packages used:
- numpy
- matplotlib
- pytest
- scipy
I have used Pip to install and manage Python packages, and the Pipfile I used is\
provided here. If you don't use Pip, just ignore it.

Run main function with following arguments to run all code:
- number_of_points: This can be 2 or 4, as these are the versions of numerical\
differentiation implemented for this task. Default is 2
- N: N is the number of intervals used in the numerical differentiation. Higher\
N gives a higher level of accuracy. Default is 10.

Optional settings can be edited in geoParameters.py. These are:
- pa: The mean pressure, currently set to 1000hPa.
- pb: The magnitude of pressure variations, currently set to 200.
- f: The Coriolis parameter, currently set to 0.0001.
- rho: Density, currently set to 1.
- L: Length scale of the pressure variations, currently set to 2400km
- ymin: Minimum space dimension, currently set to 0.
- ymax: Maximum space dimension, currently set to 10^6.