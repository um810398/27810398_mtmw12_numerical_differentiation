import numpy as np

# Input parameters for calculating the geostrophic wind
# Import this file whereever you need these parameters and functions

pa = 1e5        # the mean pressure
pb = 200        # the magnitude of pressure variations
f = 1e-4        # the Coriolis parameter
rho = 1         # density
L = 2.4e6       # length scale of the pressure variations (k)
ymin = 0.0      # minimum space dimension
ymax = 1e6      # maximum space dimension (k)

def pressure(y):
    '''The pressure and given y locations'''
    return pa + pb*np.cos(y*np.pi/L)

def uExact(y):
    '''The analytical geostrophic wind at given locations, y'''
    return pb*np.pi/(rho*f*L)*np.sin(y*np.pi/L)

