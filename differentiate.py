import numpy as np 

import geoParameters as gp

# Functions for calculating gradients
class differentiate:
    '''Differentiate numerically with either 2-point or 4-point differentiation\
    given N and pressure function.'''
    def __init__(self, N):
        '''Initialise numerical differentiation by calculating y and dy from N'''

        dy = (gp.ymax - gp.ymin)/N
        y = np.linspace(gp.ymin, gp.ymax, N+1)

        self.dy = dy
        self.y = y

    def gradient_2_point(self, p):
        '''The gradient of array p assuming points are a distance dy apart. \n
        using 2-point differences.'''

        # Initialised the array for the gradient to be the same size as f
        dpdy = np.zeros_like(p)

        # Two point differences at the end points 
        dpdy[0] = (p[1]-p[0])/self.dy
        dpdy[-1] = (p[-1]-p[-2])/self.dy

        # Centred differences for the mid-points
        for i in range(1, len(p) - 1):
            dpdy[i] = (p[i+1]-p[i-1])/(2*self.dy)
        return dpdy

    def gradient_4_point(self, p):
        '''The gradient of array p assuming points are a distance dy apart. \n
        using 4-point differences.'''

        # Initialised the array for the gradient to be the same size as f
        dpdy = np.zeros_like(p)

        # Two point differences at the end points 
        dpdy[0] = (p[1]-p[0])/self.dy
        dpdy[-1] = (p[-1]-p[-2])/self.dy

        # Third-order accuracy differences at second from end points
        dpdy[1] = (-p[3]+6*p[2]-3*p[1]-2*p[0])/(6*self.dy)
        dpdy[-2] = (2*p[-1]+3*p[-2]-6*p[-3]+p[-4])/(6*self.dy)

        # Centred differences for the mid-points
        for i in range(2, len(p) - 2):
            dpdy[i] = (-8*p[i-1]+8*p[i+1]+p[i-2]-p[i+2])/(12*self.dy)
        return dpdy