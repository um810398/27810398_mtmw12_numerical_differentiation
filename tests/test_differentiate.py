import numpy as np 
import pytest

from differentiate import differentiate
import geoParameters as gp

def test_gradient_2_point():
    '''Test simple function with 4-point differentiation. Use N=2 to test end\
    and mid points.'''
    N = 2
    differentiate_test = differentiate(N)
    p = [1, 2, 1]
    u_2_point_test = differentiate.gradient_2_point(differentiate_test, p)

    assert u_2_point_test.all() == 0

def test_gradient_4_point():
    '''Test simple function with 4-point differentiation. Use N=2 to test all\
    points with different calculations.'''
    N = 4
    differentiate_test = differentiate(N)
    p = [1, 2, 1, 2, 1]
    u_4_point_test = differentiate.gradient_4_point(differentiate_test, p)

    assert u_4_point_test.all() == 0