import pytest

from error import error

def test_calculate_error():
    '''Test error in comparing dpdy to exact value.'''
    test_error = error([1, 1.5, 1.5], [2, 2, 2])
    test = error.calculate_error(test_error)
    print(test)
    actual = [1., 0.5, 0.5]
    assert test[0] == actual[0]