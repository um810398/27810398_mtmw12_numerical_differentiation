    # Graph to compare the numerical and analytic solutions
    # plot using large fonts

import numpy as np
import matplotlib.pyplot as plt

class graph:
    '''Plots graph based on initial parameters and x and y values. Basic default\
    parameters are initialised but can be optionally changed.'''
    def __init__(self, xlabel='y (km)', ylabel='u (m/s)', label_y1='Exact', \
        name='plots/geoWindCent.pdf', label_y2='Two-point differences'):
        '''Initialise customisable graph settings, with optional inputs'''
        self.font_size = 14
        self.legend = 'best'
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.fig_name_and_location = name
        self.label_y1 = label_y1
        self.label_y2 = label_y2

    def plot_graph(self, x, y1, y2=np.empty([0])):
        '''Plot graph according to initialised settings, an x and y plot, and 
        optional y2.'''
        font = {'size': self.font_size}
        plt.rc('font', **font)

        # Plot the approximate and exact wind at y points
        plt.plot(x/1000, y1, 'k-', label=self.label_y1)
  
        if (y2.size):
            plt.plot(x/1000, y2, '*k--', label=self.label_y2, \
                ms=12, markeredgewidth=1.5, markerfacecolor='none')
        plt.legend(loc=self.legend)
        plt.xlabel(self.xlabel)
        plt.ylabel(self.ylabel)
        plt.tight_layout()
        plt.savefig(self.fig_name_and_location)
        plt.show()